package no.uib.inf101.observable;

/**
 * A listener that is notified when the value contained in an
 * ObservableValue changes. The listener should implement this
 * interface and register itself with the ObservableValue to be
 * notified when its value changes.
 *
 * @param <E> The type of the value contained in the ObservableValue.
 */
@FunctionalInterface
public interface ValueChangedListener<E> {

  /**
   * Called when the value of an ObservableValue changes.
   *
   * @param source The ObservableValue wherein the change occurred.
   * @param newValue The new value.
   * @param oldValue The previous value.
   */
  void onValueChanged(ObservableValue<E> source, E newValue, E oldValue);
}