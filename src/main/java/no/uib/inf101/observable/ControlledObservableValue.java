package no.uib.inf101.observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * An ObservableValue that allows the value to be changed. Typically,
 * the owner of a variable will have a ControlledObservableValue
 * wrapper around one of its values, and expose it as an ObservableValue
 * to the outside world.
 * <p>
 * For an example using model-view-controller:
 *
 * <pre>
 * class Model {
 *   private final ControlledObservableValue&lt;Integer&gt; score = new ControlledObservableValue&lt;&gt;();
 *
 *   public void doSomething() {
 *     this.score.setValue(this.score.getValue() + 1);
 *     // ...
 *   }
 *
 *   public ObservableValue&lt;Integer&gt; getScore() {
 *     return this.score;
 *   }
 * }
 *
 * class View extends JPanel {
 *   private final ObservableValue&lt;Integer&gt; score;
 *
 *   public View(Model model) {
 *     this.score = model.getScore();
 *     this.score.addValueChangedListener((source, newValue, oldValue) -&gt; this.repaint());
 *   }
 *
 *   public void paintComponent(Graphics g) {
 *     super.paintComponent(g);
 *     g.drawString("Score: " + this.score.getValue(), 10, 10);
 *   }
 * }
 * </pre>
 *
 * @param <E> The type of the value contained in the ObservableValue.
 */
public class ControlledObservableValue<E> implements ObservableValue<E> {
  private final List<ValueChangedListener<E>> listeners = new ArrayList<>();
  private E value;

  /**
   * Creates a new ControlledObservableValue with an initial value of null.
   */
  public ControlledObservableValue() {
    this(null);
  }

  /**
   * Creates a new ControlledObservableValue with the given initial value.
   */
  public ControlledObservableValue(E initialValue) {
    this.value = initialValue;
  }

  @Override
  public void addValueChangedListener(ValueChangedListener<E> listener) {
    this.listeners.add(listener);
  }

  @Override
  public boolean removeValueChangedListener(ValueChangedListener<E> listener) {
    return this.listeners.remove(listener);
  }

  @Override
  public E getValue() {
    return this.value;
  }

  /**
   * Sets the value of this ControlledObservableValue. If the new value
   * is different from the old value, all registered listeners will be
   * notified. Note that if the new value is *equal* to the old value,
   * no listeners will be notified, and the existing value object will
   * not be replaced by the new object (even if they are not actually
   * the same object).
   *
   * @param newValue The new value.
   */
  public void setValue(E newValue) {
    if (!Objects.equals(this.value, newValue)) {
      E oldValue = this.value;
      this.value = newValue;
      this.notifyListeners(newValue, oldValue);
    }
  }

  private void notifyListeners(E newValue, E oldValue) {
    for (ValueChangedListener<E> listener : this.listeners) {
      listener.onValueChanged(this, newValue, oldValue);
    }
  }
}
  