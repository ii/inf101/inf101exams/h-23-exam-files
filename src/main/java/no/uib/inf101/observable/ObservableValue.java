package no.uib.inf101.observable;

/**
 * An object that contains a value that can be observed. The value can be
 * retrieved by calling {@link #getValue()}. The value can be observed by
 * registering a {@link ValueChangedListener} with the
 * {@link #addValueChangedListener(ValueChangedListener)} method. The listener
 * will be notified when the value changes.
 *
 * @param <E> The type of the value contained in the ObservableValue.
 */
public interface ObservableValue<E> {

  /**
   * Adds a listener that will be notified when the value changes.
   */
  void addValueChangedListener(ValueChangedListener<E> listener);

  /**
   * Removes a listener that will be notified when the value changes.
   */
  boolean removeValueChangedListener(ValueChangedListener<E> listener);

  /**
   * Gets the current value.
   */
  E getValue();
}