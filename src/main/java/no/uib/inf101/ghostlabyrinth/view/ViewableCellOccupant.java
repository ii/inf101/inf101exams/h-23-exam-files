package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.grid.CellPosition;

import java.awt.image.BufferedImage;

/**
 * A viewable (read-only) interface of a cell occupant. The view requires the
 * following methods to be implemented by a cell occupant.
 */
public interface ViewableCellOccupant {

  /** Gets the current position of the cell occupant. */
  CellPosition getPosition();

  /** Gets an image depiction of the cell occupant. */
  BufferedImage getSprite();
}
