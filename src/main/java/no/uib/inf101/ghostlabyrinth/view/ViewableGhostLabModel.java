package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.ghostlabyrinth.model.GameState;
import no.uib.inf101.observable.ObservableValue;

/**
 * A viewable (read-only) type interface for the model in a game of
 * Ghost Labyrinth. The view requires the following interface to be
 * implemented by the model.
 */
public interface ViewableGhostLabModel {

  /**
   * Gets an observable wrapper around the game state. The game state is an
   * enum that represents the current state of the game, which decides which
   * screen should be visible (e.g. welcome screen, play screen, etc.).
   *
   * @return An observable wrapper around the game state.
   */
  ObservableValue<GameState> getGameState();

  /**
   * Gets an observable wrapper around the current game page. The game page is
   * the part of the model that represents a single play scene in the game,
   * in other words, the position of the player, the position of the ghosts,
   * the position of the door and the maze.
   *
   * @return An observable wrapper around the game page.
   */
  ObservableValue<? extends ViewableGamePage> getGamePage();

  /**
   * Gets an observable wrapper around the current level. The level is an
   * integer that represents the current level of the game. Each level has
   * a different game page.
   *
   * @return An observable wrapper around the current level.
   */
  ObservableValue<Integer> getLevel();

}
