package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.ghostlabyrinth.model.GamePageState;
import no.uib.inf101.ghostlabyrinth.model.MazeTile;
import no.uib.inf101.grid.IReadOnlyGrid;

/**
 * A viewable (read-only) interface of a game page. The view requires
 * this interface to be implemented by the model.
 */
public interface ViewableGamePage {
  /** Gets the player. */
  ViewableCellOccupant getPlayer();

  /** Gets the door. */
  ViewableCellOccupant getDoor();

  /** Gets the maze. */
  IReadOnlyGrid<MazeTile> getMaze();

  /** Gets the ghosts. */
  Iterable<? extends ViewableCellOccupant> getGhosts();

  /** Gets the state of the game page. */
  GamePageState getPageState();
}
