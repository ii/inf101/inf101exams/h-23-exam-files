package no.uib.inf101.ghostlabyrinth.view;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.Objects;

/**
 * A status line panel that displays the current status of the game.
 * The status is updated whenever the overall game state changes.
 */
public class StatusLine extends JPanel {

  private final ViewableGhostLabModel model;
  private final JLabel label = new JLabel();

  /**
   * Creates a new status line.
   *
   * @param model The model to display the status of. Must not be null.
   */
  public StatusLine(ViewableGhostLabModel model) {
    Objects.requireNonNull(model, "The model cannot be null");

    this.model = model;
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    this.add(this.label, BorderLayout.WEST);
    this.add(new JLabel(" "), BorderLayout.CENTER); // So we don't collapse if status is empty

    model.getGameState().addValueChangedListener((src, ne, ol) -> this.setStatus());
    this.setStatus();
  }

  private void setStatus() {
    switch (this.model.getGameState().getValue()) {
      case WELCOME -> this.label.setText("Press any key to start");
      case PLAYING -> this.label.setText("Level " + this.model.getLevel().getValue());
      case WON -> this.label.setText("Press any key to continue");
      case GAME_OVER -> this.label.setText("Press any key to start again");
    }
  }

}
