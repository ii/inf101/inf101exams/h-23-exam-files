package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.graphics.Inf101Graphics;

import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Graphics;

/**
 * A view that displays a single message centered in the panel.
 */
public class MessageView extends JPanel {

  private final String message;

  /**
   * Creates a new message view.
   * @param message The message to display.
   */
  public MessageView(String message) {
    this.message = message;
    this.setFocusable(true);
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setFont(new Font("Arial", Font.BOLD, 20));
    Inf101Graphics.drawCenteredString(g, this.message, this.getWidth()/2.0, this.getHeight()/2.0);
  }
}
