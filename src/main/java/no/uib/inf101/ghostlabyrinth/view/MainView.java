package no.uib.inf101.ghostlabyrinth.view;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.model.GameState;
import no.uib.inf101.observable.ObservableValue;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.util.Objects;

/**
 * The main view for the Ghost Labyrinth game. This view is managing multiple
 * sub-views, and switches between them depending on the current game state.
 * The sub-views are:
 * <ul>
 *   <li>Start view: Shown when the game is started.</li>
 *   <li>Game view: Shown when the game is being played.</li>
 *   <li>Won view: Shown in-between levels.</li>
 *   <li>Game over view: Shown when the game is lost.</li>
 *   <li>Status line: Shown at the bottom of the window at all times.</li>
 * </ul>
 * The sub-views are implemented as separate classes, and this class is
 * responsible for switching between them (except for the status line,
 * which is always visible).
 * <p>
 */
public class MainView extends JPanel {

  // Messages
  private final static String MSG_WELCOME = "Welcome to Ghost Labyrinth";
  private final static String MSG_WON = "Yay!";
  private final static String MSG_GAME_OVER = "Game Over";

  // Card name handles
  private final static String NAME_GAME_VIEW = "gameView";
  private final static String NAME_START_VIEW = "startView";
  private final static String NAME_WON_VIEW = "wonView";
  private final static String NAME_GAME_OVER_VIEW = "gameOverView";

  // Structural items
  private final JPanel centerView;
  private final CardLayout centerViewManager;

  // Card panels (the panels that are switched between)
  private final MessageView startView;
  private final MessageView wonView;
  private final MessageView gameOverView;
  private final GamePageView gameView;

  /**
   * Creates a new main view. The view is initialized with the start view.
   * The view will switch between the different sub-views depending on the
   * current game state.
   * <p>
   * In order for the view to update itself when the game state changes, it is
   * necessary to post the appropriate events to the event bus. In particular,
   * the view will react to the following event:
   * <ul>
   *   <li>{@link no.uib.inf101.ghostlabyrinth.events.GamePageChangedEvent}.
   *    This event should be posted whenever the current game page changes.
   *    The view will react by updating the game view.</li>
   * </ul>
   *
   * @param eventBus The event bus to use for event handling. Must not be null.
   * @param model The model to be displayed. Must not be null.
   */
  public MainView(EventBus eventBus, ViewableGhostLabModel model) {
    Objects.requireNonNull(eventBus, "The event bus cannot be null");
    Objects.requireNonNull(model, "The model cannot be null");

    // Initiating dynamic views
    this.gameView = new GamePageView(eventBus, model.getGamePage());
    this.startView = new MessageView(MSG_WELCOME);
    this.wonView = new MessageView(MSG_WON);
    this.gameOverView = new MessageView(MSG_GAME_OVER);
    StatusLine statusLine = new StatusLine(model);

    // Initiating center view
    this.centerView = new JPanel();
    this.centerViewManager = new CardLayout();
    this.centerView.setLayout(this.centerViewManager);
    this.centerView.add(this.startView, NAME_START_VIEW);
    this.centerView.add(this.gameView, NAME_GAME_VIEW);
    this.centerView.add(this.wonView, NAME_WON_VIEW);
    this.centerView.add(this.gameOverView, NAME_GAME_OVER_VIEW);
    this.centerViewManager.show(this.centerView, NAME_START_VIEW);

    // Initiating main view
    this.setLayout(new BorderLayout());
    this.add(this.centerView, BorderLayout.CENTER);
    this.add(statusLine, BorderLayout.SOUTH);

    // React to changes in the game state
    model.getGameState().addValueChangedListener(this::gameStateChangedHandler);
  }


  private void gameStateChangedHandler(ObservableValue<GameState> src, GameState newState, GameState oldState) {
    switch (newState) {
      case WELCOME -> {
        this.centerViewManager.show(this.centerView, NAME_START_VIEW);
        this.startView.requestFocusInWindow();
      }
      case PLAYING -> {
        this.centerViewManager.show(this.centerView, NAME_GAME_VIEW);
        this.gameView.repaint();
        this.gameView.requestFocusInWindow();
      }
      case WON -> {
        this.centerViewManager.show(this.centerView, NAME_WON_VIEW);
        this.wonView.requestFocusInWindow();
      }
      case GAME_OVER -> {
        this.centerViewManager.show(this.centerView, NAME_GAME_OVER_VIEW);
        this.gameOverView.requestFocusInWindow();
      }
    }
  }

  /** Gets the game view. */
  public GamePageView getGameView() {
    return gameView;
  }

  /** Gets the start view. */
  public MessageView getStartView() {
    return startView;
  }

  /** Gets the won view. */
  public MessageView getWonView() {
    return wonView;
  }

  /** Gets the game over -view. */
  public MessageView getGameOverView() {
    return gameOverView;
  }
}
