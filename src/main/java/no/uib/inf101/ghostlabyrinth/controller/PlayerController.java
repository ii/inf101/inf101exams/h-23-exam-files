package no.uib.inf101.ghostlabyrinth.controller;

import no.uib.inf101.ghostlabyrinth.model.GamePage;
import no.uib.inf101.grid.Direction;
import no.uib.inf101.observable.ObservableValue;

import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Objects;

/**
 * A controller that listens for key presses and moves the player accordingly.
 */
public class PlayerController extends KeyAdapter {

  private final ObservableValue<GamePage> model;

  /**
   * Creates a new player controller.
   *
   * @param view The view in which to listen for key presses. Must not be null.
   * @param model The model to update when the player moves. Must not be null.
   */
  public PlayerController(Component view, ObservableValue<GamePage> model) {
    Objects.requireNonNull(model, "The model cannot be null");
    Objects.requireNonNull(view, "The view cannot be null");

    this.model = model;
    view.addKeyListener(this);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    GamePage gamePage = this.model.getValue();
    switch (e.getKeyCode()) {
      case KeyEvent.VK_UP -> gamePage.moveAgent(gamePage.getPlayer(), Direction.NORTH);
      case KeyEvent.VK_DOWN -> gamePage.moveAgent(gamePage.getPlayer(), Direction.SOUTH);
      case KeyEvent.VK_LEFT -> gamePage.moveAgent(gamePage.getPlayer(), Direction.WEST);
      case KeyEvent.VK_RIGHT -> gamePage.moveAgent(gamePage.getPlayer(), Direction.EAST);
    }
  }
}
