package no.uib.inf101.ghostlabyrinth.controller;

import no.uib.inf101.ghostlabyrinth.model.GamePage;
import no.uib.inf101.ghostlabyrinth.model.occupants.Ghost;
import no.uib.inf101.grid.Direction;
import no.uib.inf101.observable.ObservableValue;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * A controller for all ghosts. This controller will periodically
 * check for any ghost movement to be performed, and if time is
 * due, carry out the movement decided by the ghost.
 */
public class GhostController {

  private final ObservableValue<GamePage> model;
  private final Map<Ghost, LocalDateTime> nextMoveTime = new HashMap<>();

  /**
   * Creates a new ghost controller.
   *
   * @param model the model in which to control ghosts. Must not be null.
   */
  public GhostController(ObservableValue<GamePage> model) {
    Objects.requireNonNull(model, "The model cannot be null");

    this.model = model;
    this.model.addValueChangedListener((observable, oldValue, newValue) -> {
      if (oldValue != null) {
        oldValue.getGhosts().forEach(this.nextMoveTime::remove);
      }
    });
    Timer timer = new Timer(1000/60, this::timerAction);
    timer.start();
  }

  private void timerAction(ActionEvent e) {
    GamePage gamePage = this.model.getValue();
    for (Ghost ghost : gamePage.getGhosts()) {
      if (!this.nextMoveTime.containsKey(ghost)) {
        this.nextMoveTime.put(ghost, LocalDateTime.now().plus(Duration.ofMillis(ghost.getDelay())));
      } else if (this.nextMoveTime.get(ghost).isBefore(LocalDateTime.now())) {
        Direction direction = ghost.getNextMove();
        this.nextMoveTime.put(ghost, LocalDateTime.now().plus(Duration.ofMillis(ghost.getDelay())));
        gamePage.moveAgent(ghost, direction);
      }
    }
  }
}
