package no.uib.inf101.ghostlabyrinth.controller;

import no.uib.inf101.ghostlabyrinth.model.GhostLabModel;
import no.uib.inf101.ghostlabyrinth.view.MainView;

import java.util.Objects;

/**
 * This class is responsible for creating all the controllers for the game.
 */
public class MainController {

  /**
   * Creates all the controllers for the game.
   *
   * @param model The model for the game. Must not be null.
   * @param view The view for the game. Must not be null.
   */
  public MainController(GhostLabModel model, MainView view) {
    Objects.requireNonNull(model, "The model cannot be null");
    Objects.requireNonNull(view, "The view cannot be null");

    // Controllers for screens where every keypress does the same action
    new AnyKeyPressController(view.getStartView(), model::startGame);
    new AnyKeyPressController(view.getWonView(), model::goToNextLevel);
    new AnyKeyPressController(view.getGameOverView(), model::goToWelcomeScreen);

    // Controllers for the active game
    new PlayerController(view.getGameView(), model.getGamePage());
    new GhostController(model.getGamePage());
  }
}
