package no.uib.inf101.ghostlabyrinth.model;

/**
 * The different types of tiles in the maze.
 */
public enum MazeTile {
  WALL, EMPTY;
}
