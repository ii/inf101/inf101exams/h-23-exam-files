package no.uib.inf101.ghostlabyrinth.model.occupants;

import no.uib.inf101.graphics.Inf101Graphics;
import no.uib.inf101.grid.CellPosition;

import java.awt.image.BufferedImage;

/**
 * A door is a regular cell occupant that has a door sprite.
 */
public class Door extends CellOccupant {
  private static final BufferedImage IMG = Inf101Graphics.loadImageFromResources("/sprites/door.png");

  /**
   * Creates a new door at the given position.
   * @param cellPosition The position of the door.
   */
  public Door(CellPosition cellPosition) {
    super(cellPosition);
  }

  @Override
  public BufferedImage getSprite() {
    return Door.IMG;
  }
}
