package no.uib.inf101.ghostlabyrinth.model;

/**
 * The different states of the game.
 * <ul>
 *   <li>The game starts in the WELCOME state.</li>
 *   <li>When the player starts playing, the state changes to PLAYING.</li>
 *   <li>If the player wins, the state changes to WON.</li>
 *   <li>The state may change back to PLAYING if the player continues playing.</li>
 *   <li>If the player loses, the state changes to GAME_OVER.</li>
 *   <li>The state may then change back to WELCOME.</li>
 * </ul>
 */
public enum GameState {
  WELCOME, PLAYING, WON, GAME_OVER;
}
