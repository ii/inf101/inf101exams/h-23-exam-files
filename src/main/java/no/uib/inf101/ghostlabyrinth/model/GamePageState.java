package no.uib.inf101.ghostlabyrinth.model;

/**
 * The different states of the game page.
 * <ul>
 *   <li>The game page starts in the ALIVE state.</li>
 *   <li>If the player loses, the state changes to LOST.</li>
 *   <li>If the player wins, the state changes to WON.</li>
 * </ul>
 *
 * Once the game page is in the LOST or WON state, it will never change
 * state again.
 */
public enum GamePageState {
  ALIVE, LOST, WON
}
