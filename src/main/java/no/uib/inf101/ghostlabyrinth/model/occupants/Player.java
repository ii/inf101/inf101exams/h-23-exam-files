package no.uib.inf101.ghostlabyrinth.model.occupants;

import no.uib.inf101.graphics.Inf101Graphics;
import no.uib.inf101.grid.CellPosition;

import java.awt.image.BufferedImage;

/**
 * The player is a regular cell occupant that has a player sprite.
 */
public class Player extends CellOccupant {
  private static final BufferedImage IMG = Inf101Graphics.loadImageFromResources("/sprites/face.png");

  /**
   * Creates a new player at the given position.
   * @param position The position of the player.
   */
  public Player(CellPosition position) {
    super(position);
  }

  @Override
  public BufferedImage getSprite() {
    return Player.IMG;
  }
}
