package no.uib.inf101.ghostlabyrinth.model.levels;

import no.uib.inf101.ghostlabyrinth.model.MazeTile;
import no.uib.inf101.grid.CellPosition;
import no.uib.inf101.grid.Grid;
import no.uib.inf101.grid.IReadOnlyGrid;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A level is a maze with a player, a door and a number of ghosts. It
 * represents the initial state of a game page.
 *
 * @param maze The maze.
 * @param playerPosition The player's position.
 * @param doorPosition The door's position.
 * @param ghostPositions The ghosts' positions.
 */
public record Level(
    IReadOnlyGrid<MazeTile> maze,
    CellPosition playerPosition,
    CellPosition doorPosition,
    Iterable<CellPosition> ghostPositions) {

  /**
   * Load a level. The level number is used to find the level file
   * "levelN.txt" in the resources folder. If the level file is not
   * found, the default level will be returned and a warning will be
   * printed to stderr.
   *
   * @param n The level number.
   * @return The level.
   */
  public static Level load(int n) {
    if (n <= 0) {
      return getDefaultLevel();
    }

    List<String> lines;
    try {
      lines = loadResource("level" + n + ".txt");
    } catch (NullPointerException | UncheckedIOException e) {
      System.err.println("Level " + n + " not found; will use default level.");
      return getDefaultLevel();
    }

    int rows = lines.size();
    int cols = lines.get(0).length();

    Grid<MazeTile> maze = new Grid<>(rows, cols, MazeTile.EMPTY);
    CellPosition playerPosition = new CellPosition(1, 0);
    CellPosition doorPosition = new CellPosition(0, 1);
    List<CellPosition> ghostPositions = new ArrayList<>();

    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        char c = lines.get(row).charAt(col);
        switch (c) {
          case '-' -> { /* do nothing */ }
          case '#' -> maze.set(new CellPosition(row, col), MazeTile.WALL);
          case 'P' -> playerPosition = new CellPosition(row, col);
          case 'D' -> doorPosition = new CellPosition(row, col);
          case 'Y' -> ghostPositions.add(new CellPosition(row, col));
          default -> throw new IllegalArgumentException("Unknown character: '" + c + "'");
        }
      }
    }

    return new Level(maze, playerPosition, doorPosition, ghostPositions);
  }

  private static List<String> loadResource(String filename) {
    List<String> lines = new ArrayList<>();
    BufferedReader reader = new BufferedReader(new InputStreamReader(
        Objects.requireNonNull(Level.class.getResourceAsStream(filename)),
        StandardCharsets.UTF_8
    ));
    reader.lines().forEach(lines::add);
    lines.replaceAll(String::strip);
    lines.removeIf(String::isEmpty);
    return lines;
  }

  private static Level getDefaultLevel() {
    Grid<MazeTile> maze = new Grid<>(10, 10, MazeTile.EMPTY);
    maze.set(new CellPosition(0, 3), MazeTile.WALL);
    maze.set(new CellPosition(1, 3), MazeTile.WALL);
    maze.set(new CellPosition(2, 3), MazeTile.WALL);
    CellPosition playerPosition = new CellPosition(0, 0);
    CellPosition doorPosition = new CellPosition(9, 9);
    List<CellPosition> ghostPositions = Arrays.asList(new CellPosition(5, 5));
    return new Level(maze, playerPosition, doorPosition, ghostPositions);
  }
}
