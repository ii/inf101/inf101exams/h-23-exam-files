package no.uib.inf101.ghostlabyrinth.model;

import no.uib.inf101.eventbus.EventBus;
import no.uib.inf101.ghostlabyrinth.events.GamePageChangedEvent;
import no.uib.inf101.ghostlabyrinth.view.ViewableGhostLabModel;
import no.uib.inf101.observable.ControlledObservableValue;
import no.uib.inf101.observable.ObservableValue;

import java.util.Objects;
import java.util.Random;

/**
 * The main model for the Ghost Labyrinth game. This class is
 * responsible for managing the overall game state and swapping between
 * different game pages (levels).
 * <p>
 * The model consists of variables for gameState and level, as well as
 * a gamePage variable that represents the current game page. The game
 * page is a separate part of the model that represents a single play
 * scene in the game, in other words, the position of the player, the
 * position of the ghosts, the position of the door and the maze. Each
 * level has a different game page.
 * <p>
 * Whenever the game page changes, a {@link GamePageChangedEvent} should
 * be posted on the event bus. This event should be posted by the game
 * page itself.
 * <p>
 * Changes to the game state, level and the active game page are
 * subscribable through {@link ObservableValue} wrappers.
 * <p>
 */
public class GhostLabModel implements ViewableGhostLabModel {

  private final Random random;
  private final EventBus eventBus;

  // State variables; screen and level management
  private final ControlledObservableValue<GameState> gameState = new ControlledObservableValue<>(GameState.WELCOME);
  private final ControlledObservableValue<Integer> level = new ControlledObservableValue<>(0);
  private final ControlledObservableValue<GamePage> gamePage = new ControlledObservableValue<>(null);

  /**
   * Creates a new Ghost Labyrinth model.
   *
   * @param eventBus The event bus to use for posting events. Must not be null.
   * @param random  The random number generator to use. Must not be null.
   */
  public GhostLabModel(EventBus eventBus, Random random) {
    Objects.requireNonNull(eventBus, "The event bus cannot be null");
    Objects.requireNonNull(random, "The random number generator cannot be null");

    this.eventBus = eventBus;
    this.random = random;
    this.registerEventHandlers();
    this.init();
  }

  private void registerEventHandlers() {
    this.eventBus.register(event -> {
      if (event instanceof GamePageChangedEvent e) {
        GamePageState pageState = this.gamePage.getValue().getPageState();
        switch (pageState) {
          case WON -> this.gameState.setValue(GameState.WON);
          case LOST -> this.gameState.setValue(GameState.GAME_OVER);
        }
      }
    });
  }

  private void init() {
    this.gameState.setValue(GameState.WELCOME);
    this.level.setValue(0);
    this.gamePage.setValue(new GamePage(this.eventBus, this.random, this.level.getValue()));
  }

  // GETTERS

  @Override
  public ObservableValue<GamePage> getGamePage() {
    return this.gamePage;
  }

  @Override
  public ObservableValue<Integer> getLevel() {
    return this.level;
  }

  @Override
  public ObservableValue<GameState> getGameState() {
    return this.gameState;
  }

  // METHODS FOR MANAGING THE GAME STATE

  /**
   * Starts the game. This method can only be called when the game is in the
   * welcome state, otherwise an exception will be thrown. This method will
   * initialize the game to level 0 and set the game state to playing.
   *
   * @throws IllegalStateException if the game is not in the welcome state.
   */
  public void startGame() {
    if (!Objects.equals(GameState.WELCOME, this.gameState.getValue())) {
      throw new IllegalStateException("Can only start game when the game is in the welcome state");
    }
    this.init();
    this.gameState.setValue(GameState.PLAYING);
  }

  /**
   * Goes to the next level. This method can only be called when the game is
   * in the won state, otherwise an exception will be thrown. This method will
   * increment the level and initialize the game page to the new level. The
   * game state will be set to playing.
   *
   * @throws IllegalStateException if the game is not in the won state.
   */
  public void goToNextLevel() {
    if (!Objects.equals(GameState.WON, this.gameState.getValue())) {
      throw new IllegalStateException("Can only go to next level when the game is won");
    }
    this.level.setValue(this.level.getValue() + 1);
    this.gamePage.setValue(new GamePage(this.eventBus, this.random, this.level.getValue()));
    this.gameState.setValue(GameState.PLAYING);
  }

  public void goToWelcomeScreen() {
    this.gameState.setValue(GameState.WELCOME);
  }
}
