# Eksamen i INF101 høsten 2023

Oppgavene er beskrevet i Inspera (vurdering.uib.no) på både bokmål og nynorsk. For å gjøre det litt enklere for deg, har vi kopiert inn en bokmålsversjon av oppgavene her i tillegg. Merk at tekstoppgaver må besvares i Inspera (vurdering.uib.no), mens for kodeoppgaver skal du gjøre endringer i dette repositoriet, som du deretter laster opp til Inspera.

Alle oppgavene i denne eksamen omhandler spillet «Ghost Labyrinth», som er delvis implementert. Du kan starte programmet ved å kjøre main-metoden i [MainGhostLab.java](./src/main/java/no/uib/inf101/ghostlabyrinth/MainGhostLab.java).


Illustrasjon av ferdig program:

![Illustrasjon av ferdig program](./doc/img/ghostlab.gif)


## Oppgave 1: forståelse (15 poeng)

I denne oppgaven skal vi bli litt kjent med kildekoden som allerede er skrevet. Oppgaven besvares i sin helhet på vurdering.uib.no. Sensor vil se etter om du demonstrerer en helhetlig forståelse av kildekoden.

**Del A:** Du husker sikkert at det er viktig å kalle på «repaint» hver gang det skjer en endring i modellen; hvis ikke vil vi ikke kunne se endringen på skjermen. Gitt at du er i midt i et aktivt spill: forklar programflyten i kildekoden fra en person klikker på en piltast for å flytte spilleren frem til repaint blir kalt.

> Tips: benytt debuggeren og klikk deg gjennom hva som skjer steg for steg. Du kan begynne med å sette et breakpoint på linjen `GamePage gamePage = this.model.getValue();` i PlayerController.java

**Del B:** Når spilleren kommer seg til målet, vises en skjerm som forteller at det er mulig å gå videre til neste brett. Da kan hen trykke på en hvilken som helst tast for å gå videre. Gi en kort beskrivelse av hvordan modellen er bygget opp, og hva slags prosesser i kildekoden som skjer når man går videre til neste nivå.

> Tips for å bli kjent med kontrollflyten:
> - Start programmet i debug-modus. Spill ferdig første nivå uten å sette noe breakpoint, slik at du kommer frem til skjermen som forteller deg at du er videre til neste nivå.
> - Sett nå et breakpoint i AnyKeyPressController.java på linjen som sier `this.action.run();`. Gå tilbake til programmet som kjører og trykk en tast. Debuggeren skal nå stoppe deg på linjen hvor du har satt et breakpoint.
> - Trykk deg steg-for-steg videre gjennom kildekoden ved å benytte «step into» og «step over» -knappene i debuggeren.

## Oppgave 2: spøkelser gjennom vegger (15 poeng)

Direktøren i «The Ghost Labyrinth Company» ønsker å forbedre spillet. Den første endringen er at spøkelser (men ikke spilleren) skal ha mulighet til å gå gjennom vegger.

**Del A:** foreslå minst to ulike måter å endre koden slik at den får ønsket funksjonalitet. Begrunn hvilken variant du foretrekker. Dette besvares på vurdering.uib.no.

**Del B:** utfør endringen du valgte i del A i kildekoden.

## Oppgave 3: svarte spøkelser (20 poeng)

Direktøren i «The Ghost Labyrinth Company» ønsker å forbedre spillet ytterligere ved å ha to ulike typer spøkelser; gule spøkelser oppfører seg som før, mens *svarte* spøkelser alltid skal gå direkte mot spilleren. 


- Svarte spøkelser skal alltid gå til den ruten som er nærmest spilleren i *luftlinje*.
- Fordi svarte spøkelser er så smarte, skal de ha litt langsommere oppdateringsintervall enn de gule spøkelsene: 400 millisekunder.

**Del A:** utfør endringer i kildekoden slik at den får ønsket funksjonalitet. Legg til minst ett svart spøkelse i hvert nivå (inkludert nivå 0). Tilpass testene slik at de fremdeles passerer.

**Del B:** beskriv hvordan du endret koden (eller planla å endre koden) i steg A. Drøft også alternative løsninger du vurderte, og eventuelt hvorfor de er bedre eller dårligere enn hva du valgte. Besvares på i Inspera (vurdering.uib.no).

## Oppgave 4: oppsamling av nøkler (10 poeng)

Direktøren i «The Ghost Labyrinth Company» ønsker å forbedre spillet ytterligere ved å legge til nøkler man må samle opp. Det skal ikke være mulig å åpne døren uten at man har samlet inn alle nøklene.

**Del A:** utfør endringene i kildekoden slik at den får ønsket funksjonalitet.

**Del B:** beskriv hvordan du endret koden for å oppnå ønsket funksjonalitet, eller hvordan du planla å gjøre det. Besvares i Inspera (vurdering.uib.no).

## Oppgave 5: ytterligere forbedringer (5 poeng)

Direktøren i «The Ghost Labyrinth Company» er svært fornøyd med innsatsen din, og gir deg frihet til å forbedre spillet slik du selv ønsker. Forbedringer kan være ny funksjonalitet, forbedringer av kodekvalitet, bedre tester eller noe helt annet.

**Del A:** utfør frivillige endringer.

**Del B:** beskriv hvilke endringer du har gjort, og hvordan du har endret kildekoden (besvares i Inspera/vurdering.uib.no).
